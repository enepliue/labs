class Triangle extends Figure {
    double sideA;
    double sideB;
    double sideC;

    private Triangle (int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        Perimeter();
        Area();
    }
    public static void main(String[] args) {
        Triangle myTriangle = new Triangle(3, 6,6);
        System.out.println("This is triangle area: " + myTriangle.Area());
        System.out.println("This is triangle perimeter: " + myTriangle.Perimeter());
    }
    @Override
    double Perimeter() {
        double perimeter = sideA + sideB + sideC;
        double local = perimeter;
        return local;
    }
    @Override
    double Area() {
        double perimeter = (sideA + sideB + sideC)/2;
        double area = Math.sqrt(perimeter * (perimeter - sideA) * (perimeter - sideB) * (perimeter - sideC));
        return area;
    }
}