public class Human {
    String surname;
    String name;
    String patronymic;
    final String yearOfBirth;
    String country;
    String city;
    String street;
    String house;
    String apartment;
    String zip;
    Human[] children;
    Human spouse;

    public Human(String surname, String name, String patronymic, String yearOfBirth, Human[] children, Human spouse) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.yearOfBirth = yearOfBirth;
        this.children = children;
        this.spouse = spouse;
    }

    public Human(String surname, String name, String patronymic, String yearOfBirth) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.yearOfBirth = yearOfBirth;
    }
    public Human(String surname, String name, String yearOfBirth) {
        this(surname, name, "none", yearOfBirth);
    }

    public Human(String yearOfBirth) {
        this("none", "none", "none", yearOfBirth);
    }
    public Human(String yearOfBirth, String country, String city, String street, String house, String apartment,
                 String zip) {
        this.yearOfBirth = yearOfBirth;
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.zip = zip;
    }

    public static void main(String[] args) {
        Address newAddress = new Address("Россия", "Подольск", "Ленина","75", "79",
                "394500");
        System.out.println("Страна: " + newAddress.country + "\nГород: " + newAddress.city + "\nДом: " + newAddress.house
                + "\nКвартира: " + newAddress.apartment + "\nИндекс: " + newAddress.zip);

        Human spouse = new Human("Иванова", "Наталья", "Андреевна", "1988");
        Human[] children = new Human[2];
        children[0] = new Human("Иванов", "Сергей", " Иванович", "1999");
        children[1] = new Human("Иванов", "Петр", "Иванович", "2000");

        Human firstHuman = new Human("Иванов", "Иван", "Иванович", "1985",
                children, spouse);
        System.out.println("Фамилия: " + firstHuman.surname + "\nИмя: " + firstHuman.name +
                "\nОтчество: " + firstHuman.patronymic + "\nГод рождения: " + firstHuman.yearOfBirth + "\nЖена" +
                        "\nФамилия: " + firstHuman.spouse.surname + "\nИмя: " + firstHuman.spouse.name +
                        "\nОтчество: " + firstHuman.spouse.patronymic + "\nГод рождения: " +
                        firstHuman.spouse.yearOfBirth + "\nДети" + "\nФамилия: " + firstHuman.children[0].surname +
                "\nИмя: " + firstHuman.children[0].name + "\nОтчество: " + firstHuman.children[0].patronymic +
                "\nДата рождения: " + firstHuman.children[0].yearOfBirth + "\nФамилия: " +
                firstHuman.children[1].surname + "\nИмя: " + firstHuman.children[1].name + "\nОтчество: " +
                firstHuman.children[1].patronymic + "\nДата рождения: " + firstHuman.children[1].yearOfBirth);

        Human secondHuman = new Human("Gans", "Stolz", "1334");
        Human thirdHuman = new Human("2020");
    }
}


