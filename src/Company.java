public class Company {
    private String name;
    private String address;
    private String sphere;


    public Company(String name, String address, String sphere) {
        this.name = name;
        this.address = address;
        this.sphere = sphere;
    }

    public static void main(String[] args) {
        Company firstCompany = new Company("T-Systems", "Saint-Petersburg", "IT");
        Employee[] employees = new Employee[4];

        employees[0] = new Employee("Tony", "QA Engineer",  40000);
        String testerName = employees[0].getName();
        String testerPosition = employees[0].getPosition();
        double testerSalary = employees[0].getSalary();

        employees[1] = new Employee("Kelly", "CEO", 100000);
        String directorName = employees[1].getName();
        String directorPosition = employees[1].getPosition();
        double directorSalary = employees[1].getSalary();

        employees[2] = new Employee("Harry", "Owner", 500000);
        String ownerName = employees[2].getName();
        String ownerPosition = employees[2].getPosition();
        double ownerSalary = employees[2].getSalary();

        employees[3] = new Employee("Johny", "Developer", 60000);
        String developerName = employees[3].getName();
        String developerPosition = employees[3].getPosition();
        double developerSalary = employees[3].getSalary();

        double salary[] = new double[]{employees[0].getSalary(), employees[1].getSalary(),
                employees[2].getSalary(), employees[3].getSalary()};

        Company secondCompany = new Company("JetBrains", "Prague", "IT");
        Employee[] j_bEmployees = new Employee[3];

        j_bEmployees[0] = new Employee("Bob", "Middle Developer", 70000);
        String name1 = employees[0].getName();
        String position1 = employees[0].getPosition();
        double salary1 = employees[0].getSalary();

        j_bEmployees[1] = new Employee("Helga", "Head of department", 600000);
        String name3 = employees[1].getName();
        String position3 = employees[1].getPosition();
        double salary3 = employees[1].getSalary();

        j_bEmployees[2] = new Employee("Mary", "Solution Designer", 80000);
        String name4 = employees[2].getName();
        String position4 = employees[2].getPosition();
        double salary4 = employees[2].getSalary();

        Company thirdCompany = new Company("Dell", "USA", "Digital Technology");
        Employee[] dellEmployess = new Employee[2];

        dellEmployess[0] = new Employee("Denny", "Head of company", 600000);
        String name5 = employees[0].getName();
        String position5 = employees[0].getPosition();
        double salary5 = employees[0].getSalary();

        dellEmployess[1] = new Employee("Max", "Sales manager", 80000);
        String name6 = employees[1].getName();
        String position6 = employees[1].getPosition();
        double salary6 = employees[1].getSalary();

        double totalSalary[] = new double[]{
                employees[0].getSalary(), employees[1].getSalary(),
                employees[2].getSalary(), employees[3].getSalary(), j_bEmployees[0].getSalary(),
                j_bEmployees[1].getSalary(), j_bEmployees[2].getSalary(), dellEmployess[0].getSalary(),
                dellEmployess[1].getSalary()
        };

        nameCeo(employees);
        nameEngineer(employees);
        nameDeveloper(employees);
        nameOwner(employees);
        getSalary(salary);
        getTotalSalary(totalSalary);
    }

    public static void nameCeo(Employee[] employees) {
        if (employees[1].getPosition().equals("CEO")) {
            System.out.println("CEO name: " + employees[1].getName());
        }
    }

    private static void nameDeveloper(Employee[] employees) {
        if (employees[3].getPosition().equals("Developer")) {
            System.out.println("CEO name: " + employees[3].getName());
        }
    }

    private static void nameEngineer(Employee[] employees) {
        if (employees[0].getPosition().equals("QA Engineer")) {
            System.out.println("QA Engineer name: " + employees[1].getName());
        }
    }

    protected static void nameOwner(Employee[] employees) {
        if (employees[2].getPosition().equals("Owner")) {
            System.out.println("Owner name: " + employees[2].getName());
        }
    }

    public static void getSalary(double[] salary) {
        double maxValue = 0;
        double sum = 0;
        double averageSalary = 0;
        for (int i = 0; i < salary.length; i++) {
            if (salary[i] > maxValue) {
                maxValue = salary[i];
            }
            sum += salary[i];
            averageSalary = sum / salary.length;
        }
        System.out.println("Max salary in company: " + maxValue);
        System.out.println("Average salary in company: " + averageSalary);
    }
    public static void getTotalSalary(double[] totalSalary) {
        double maxValue = 0;
        double sum = 0;
        double averageSalary = 0;
        for (int i = 0; i < totalSalary.length; i++) {
            if (totalSalary[i] > maxValue) {
                maxValue = totalSalary[i];
            }
            sum += totalSalary[i];
            averageSalary = sum / totalSalary.length;
        }
        System.out.println("Max total salary: " + maxValue);
        System.out.println("Average total salary: " + averageSalary);
    }
}