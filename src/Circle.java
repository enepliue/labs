class Circle extends Figure {
    double radius = 1;
    double pi = Math.PI;

    private Circle(double radius, double pi) {
        this.radius = radius;
    }
    public static void main(String[] args) {
        Circle myCircle = new Circle(1, Math.PI);
        System.out.println("This is triangle area: " + myCircle.Area());
        System.out.println("This is triangle perimeter: " + myCircle.Perimeter());
    }
    double Perimeter() {
        double perimeter = 2 * pi * radius;
        double local = perimeter;
        return local;
    }
    double Area() {
        double area = pi * radius * radius;
        double local = area;
        return local;
    }
}