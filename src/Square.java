import java.rmi.MarshalException;

class Square extends Figure {
    double sideA;

    private Square (double side) {
        this.sideA = side;
        this.Area();
        this.Perimeter();
    }
    public static void main(String[] args) {
        Square mySquare = new Square(1);
        System.out.println("This is square area: " + mySquare.Area());
        System.out.println("This is square perimeter: " + mySquare.Perimeter());
    }
    @Override
    double Perimeter() {
        double perimeter = sideA * 4;
        double local = perimeter;
        return local;
    }
    @Override
    double Area() {
        double area = sideA * sideA;
        double local = area;
        return local;
    }
}