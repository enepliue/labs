public class Address {
    String country;
    String city;
    String street;
    String house;
    String apartment;
    String zip;
    Human[] children;

    Address(String country, String city, String street, String house, String apartment, String zip) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.zip = zip;
    }

    Address(String country, String city, String street, String house, String apartment) {
        this(country, city, street, house, apartment, "0");
    }

    public static void main(String[] args) {
        Address firstAddress = new Address("Poland", "Brzesko", "ul. Kościuszki", "74",
                "254", "32-800");
        Address secondAddress = new Address("Germany", "Erfurt", "Gotthardstrasse", "64", "154");
        System.out.println("Country: " + firstAddress.country + "\nCity: " + firstAddress.city + "\nStreet: " +
                firstAddress.street + "\nHouse: " + firstAddress.house + "\nApartment: " + firstAddress.apartment + "\nZip: " +
                firstAddress.zip);
        System.out.println();
        System.out.println("Country: " + secondAddress.country + "\nCity: " + secondAddress.city + "\nStreet: " +
                secondAddress.street + "\nHouse: " + secondAddress.house + "\nApartment: " + secondAddress.apartment);
    }
}
