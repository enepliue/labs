public class Wrappers {
    public static void main(String[] args) {
        //(1) Распарсите сроку doubleNumber в переменную d, используя методы parseDouble и valueOf() класса Double.
        // В чем разница этих методов и какая неявная операция происходит при использовании одного из них?
        String doubleNumber = "2.5";
        System.out.println("(1): " + "\nvalueOf " + valueOf(doubleNumber) + ", " + "parseDouble " +
                parseDouble(doubleNumber));

        //(2) Вызовите метод incrementInt(...). Какая из двух реализаций будет вызвана и почему именно она?
        int i = 0;
        incrementInt(i);
        incrementInt(i);

        //(3) Вызовите метод incrementInt(...) для iWrapper. Изменилось ли значение переменной после вызова метода?
        //Попробуйте объяснить результат
        Integer iWrapper = 0;
        incrementIntt(iWrapper);
        incrementIntt(iWrapper);

        //(4) Пропишите в строчка 27-30 явно преобразования, которые делает компилятор неявно
        // Почему строчка 31 не компилируется?
        double d = 2.4;
        int ii = 1;
        float f = 1.6f;
        long l = 1000;
        byte b = 1;
        short s = 1;
        d = d + (double)s;
        f = (float)b + f;
        l = l + (long)i;
        ii = (int)b + (int)s;
        //b = b + s; Диапазон byte меньше, чем диапазон short. Если необходимо преобразовать тип данных byte в short,
        // нужно это сделать явно.

        //(5) Напишите простую иерархию классов
        // Создайте по одному объекту классов наследников. Приведите их к базовому классу и обратно, аналогично примеру ниже
        /*
        Cat cat = new Cat();
        Bear bear = new Bear();
        Animal a = ??? cat;
        cat = ??? a;
        a = ??? bear;
        bear = ??? a;

        class Food {
        }
        class Cheese extends Food {
        }
        class Onion extends Food {
        }

        Cheese cheese = new Cheese();
        Onion onion = new Onion();

        Food food = Cheese cheese;
        cheese = (Cheese) food;

        food = (Food) onion;
        onion = (Onion) food;
         */
    }

    //(1)
    public static double valueOf(String doubleNumber) {
        double d = Double.valueOf(doubleNumber);
        return d;
    }

    public static double parseDouble(String doubleNumber) {
        double d = Double.parseDouble(doubleNumber);
        return d;
        /*
        Оба метода преобразуют значение с типом данных String в значение с типом данных double. Отличие в том,
        что метод valueO() неявно преобразеут примитивный тип double в класс-обертку Double, содержащий примитивное
        значение double. Метод parseDouble() преобразует значение типа String в значение с примитивным типом double.
        */
    }
    //(2)
    public static void incrementInt(int i) {
        i++;
        System.out.println(i);
    }
    public static void incrementInt(Integer i) {
        i++;
        System.out.println(i);
        /*
        Будет вызвана реализация public static void incrementInt(int i), так как для того, чтобы вызвать метод
        incrementInt с переданным ему значением Integer i, необходимо сначала создать объект Integer i и
        проинициализаровать его (передать примтивное значение int = 0). В этом случае примитив завернется в обертку
        и функция будет реализована. Но так как в задана переменная i с примитивным типом int,
        то будет вызвана функция public static void incrementInt(int i).
        */
    }
    //(3)
    public static void incrementIntt(Integer iWrapper) {
        iWrapper++;
        System.out.println(iWrapper);
    }
    public static void incrementIntt(int iWrapper) {
        iWrapper++;
        System.out.println(iWrapper);
        /*
        Будет вызвана реализация incrementIntt(Integer iWrapper), потому что объявлена переменная Integer. Внутри метода
        класс-обертка будет распакован, метод отработает с примитивным значением и вернет результат.
        */
    }
}

