public class Fibonacci {
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        int fibPosition = 10;
        System.out.println("fibInCycle: " + fibInCycle(fibPosition));
        System.out.println("Cycle start time: " + startTime);
        System.out.println("Cycle end time: " + calculatedRunningTime(startTime));
        System.out.println();
        long recursiveStartTime = System.nanoTime();
        System.out.println("fibonacciRecursive: " + fibonacciRecursive(fibPosition));
        System.out.println("Recursive start time: " + recursiveStartTime);
        System.out.println("Recursive end time: " + calculatedRunningTime(recursiveStartTime));
    }
    public static int fibInCycle(int fibPosition) {
        int fibCurrent = 1;
        int fibPrevious = 1;
        for (int i = 2; i < fibPosition; i++) {
            int local = fibCurrent;
            fibCurrent += fibPrevious;
            fibPrevious = local;
        }
        return fibCurrent;
    }
    public static int fibonacciRecursive(int fibPosition) {
        if (fibPosition == 1) {
            return 1;
        }
        else if (fibPosition == 2) {
            return 1;
        }
        return fibonacciRecursive(fibPosition - 1) + fibonacciRecursive(fibPosition - 2);
    }
    public static long calculatedRunningTime(long startTime) {
        long local = startTime + System.nanoTime();
        return local;
    }
}