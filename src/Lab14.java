import java.util.ArrayList;

public class Lab14 {
    public static void main(String[] args) {
        String name = "Alice";
        System.out.println("Task 1 \"politeMethod\": " + politeMethod(name));

        String word = "word";
        String wrap = "(())";
        System.out.println("Task 2 \"makeWrapWord\": " + makeWrapWord(word, wrap));

        System.out.println("Task 3 \"half\": " + half(politeMethod(name)));

        String s = "Object-oriented design has abbreviation ood";
        System.out.println("Task 4 \"endsOod\": " + endsOod(s));

        char c = 'o';
        System.out.println("Task 5 \"charCounter\": " + charCounter(s, c));

        double d = 16.76;
        System.out.println("Task 6 \"oddLength\": " + oddLength(d));

        String stringJava = "Java programmers love Java";
        String searchString = "java";
        stringCounter(stringJava, searchString);
        System.out.println("Task 7 \"stringCounter\": " + stringCounter(stringJava, searchString));

    }
    public static String politeMethod(String name) {
        String s = "Hello " + name + "!";
        return s;
    }

    public static String makeWrapWord(String word, String wrap) {
        return wrap.substring(0, 2) + word + wrap.substring(2, 4);
    }

    public static String half(String name) {
        int len1 = name.length();
        return name.substring(0, len1 / 2);
    }

    public static boolean endsOod(String s) {
        boolean statement;
        statement = s.endsWith("ood");
        return statement;
    }

    public static  int charCounter(String s, char c) {
        char[] arrayString = s.toCharArray();
        int count = 0;
        for (int i = 0; i < arrayString.length; i++) {
            if (s.charAt(i) == c) count++;
        }
        return count;
    }
    public static boolean oddLength(double d) {
        String str = Double.toString(d);
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            count++;
        }
        boolean expression = (count % 2 != 0);
        return expression;
    }
    public static int stringCounter(String stringJava, String searchString) {
        String stringJavaCopy = stringJava.toLowerCase();
        String searchStringCopy = searchString.toLowerCase();
        int index = stringJavaCopy.indexOf(searchStringCopy);
        int count = 0;
        while (index >= 0) {
            count++;
            index = stringJavaCopy.indexOf(searchStringCopy, index + 1);
        }
        return count;
    }
}