public class Baker {
    ElectricOven electricOven = new ElectricOven();
    GasOven gasOven = new GasOven();

    public void turnOn() {
    }
    public void turnOff() {
    }
    public void increaseTemp() {
    }
    public void decreaseTemp() {
    }
    public void calculateTime() {
    }
    public void selectBakeMode() {
    }
    public static void main(String[] args) {
        Baker baker = new Baker();
    }
}