import stub.*;
import test.*;
import animal.*;
import robot.*;

public class Environment {
    public static void main(String[] args) {
        dog();
        cat();
        terminator();
        simulator();
    }
    public static void simulator() {
        System.out.println(TestCase.startTestCase());
        System.out.println(TestChain.startTestChain());
        System.out.println(Simulator.startSimulator());
    }
    public static void dog() {
        animal.Cat.sayHello();
    }
    public static void cat() {
        animal.Dog.sayHello();
    }
    public static void terminator() {
        robot.Terminator.sayHello();
    }
}
