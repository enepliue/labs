public class Oven {
    public void turnOn() {
    }
    public void turnOff() {
    }
    public void calculateTime() {
    }
    public void increaseTemp() {
    }
    public void decreaseTemp() {
    }
    public void selectBakeMode() {
    }
    public void onState() {
    }
    public void offState() {
    }
    public void setTempHigher() {
    }
    public void setTempLower() {
    }
    public void setBakingTime() {
    }
    public void modeBakeCookies() {
    }
    public void modeBakePies() {
    }
    public void modeBakeBread() {
    }
    public void modeBakePizza() {
    }
}
