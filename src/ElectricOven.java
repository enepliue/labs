class ElectricOven extends Oven {
    public void main(String[] args) {
        ElectricOven electricOven = new ElectricOven();
    }
    @Override
    public void turnOn() {
    }
    @Override
    public void turnOff() {
    }
    @Override
    public void increaseTemp() {
    }
    @Override
    public void decreaseTemp() {
    }
    @Override
    public void calculateTime() {
    }
    @Override
    public void selectBakeMode() {
    }
    @Override
    public void onState() {
    }
    @Override
    public void offState() {
    }
    @Override
    public void setTempHigher() {
    }
    @Override
    public void setTempLower() {
    }
    @Override
    public void setBakingTime() {
    }
    @Override
    public void modeBakeCookies() {
    }
    @Override
    public void modeBakePies() {
    }
    @Override
    public void modeBakeBread() {
    }
    @Override
    public void modeBakePizza() {
    }
}